window.scroll(0, 300)

var isScrolling;

document.onscroll = () => processMenu(document.getElementById('mainMenu')) ;
const processMenu = parent => {
    let children = [].slice.call(parent.children);
    children.forEach(element => element.className = "");
    visibleChildren = children.filter(isScrolledToVisible);
    
    if (visibleChildren.length == 0) return; // DO NOT CONTINUE IF THERE ARE NO ELEMENTS LEFT TO PROCESS
    activeMenu = visibleChildren[0];

    activeMenu.className = "active";
}

isScrolledToVisible = element => {
    let rect = element.getBoundingClientRect();
    return (rect.top >= 0) && (rect.bottom <= window.innerHeight);
}


// TODO: Move away from Timeout
window.addEventListener('scroll', function ( event ) {
	window.clearTimeout(isScrolling);
	isScrolling = setTimeout(() => {
        window.location.href = 'index.html?page=' + document.querySelector('#mainMenu > div.active').innerHTML.split(' ')[2]
	}, 200);
}, false);


window.scroll(0, 310);